# use RStudio's Public Package Manager to download binary packages for Ubuntu 18.04 (Bionic)
options(repos = c(CRAN = "https://packagemanager.rstudio.com/all/__linux__/bionic/latest"))

# source: https://kevinushey.github.io/blog/2015/02/02/rprofile-essentials/
# and https://github.com/csgillespie/rprofile
options(
  width = as.integer(system2("tput", args = "cols", stdout = TRUE)),
  warnPartialMatchArgs = TRUE, # warn on partial matches
  warnPartialMatchDollar = TRUE,
  warnPartialMatchAttr = TRUE,
  warn = 2, # warnings are errors
  lifecycle_verbosity = "error", # for packages using {lifecycle}
  useFancyQuotes = FALSE, # fancy quotes lead to copy+paste bugs
  papersize = "a4",
  continue = " ",
  max.print = 100,
  setWidthOnResize = TRUE,
  show.error.locations = TRUE,
  showWarnCalls = TRUE,
  showErrorCalls = TRUE,
  Ncpus = parallelly::availableCores(), # see https://www.jumpingrivers.com/blog/speeding-up-package-installation/ and https://www.jottr.org/2022/12/05/avoid-detectcores/
  datatable.print.class = TRUE,
  datatable.print.keys = TRUE,
  shiny.reactlog = TRUE,
  progressr.enable = TRUE, # let {progressr} report on progress also in non-interactive mode
  checkPackageLicense = TRUE
)

# Do not enrich warnings with {rlang} due to conflicts with {colorout}
rlang::global_entrace(class = c("error", "message"))
options(rlang_backtrace_on_error = "full")

if (interactive()) {
  # if available, use {progressr} to report on progress from every call
  progressr::handlers(global = TRUE)

  # instruct {progressr} to use progress bar from {cli}
  progressr::handlers("cli")

  # print functions colorful
  suppressMessages(prettycode::prettycode())

  # print tables colorful
  .S3method("print", "data.frame", colorDF::print_colorDF)
  setHook(
    packageEvent("pillar", "onLoad"),
    function(...) .S3method("print", "tbl", colorDF::print_colorDF)
  )
  setHook(
    packageEvent("data.table", "onLoad"),
    function(...) .S3method("print", "data.table", colorDF::print_colorDF)
  )
  options(colorDF_tibble_style = TRUE)

  # print everything else colorful
  colorout::setOutputColors(normal = 0, number = 2, string = 1, const = 5, verbose = FALSE)

  if (Sys.getenv("RSTUDIO") != "1" || Sys.getenv("RSTUDIO_TERM") != "") {
    # print help pages colorful
    rdoc::use_rdoc()
  }
}

# If R is run inside a Jupyter kernel
if (Sys.getenv("JPY_PARENT_PID") != "") {
  # Print immediately
  trace(what = "print", where = getNamespace("base"), exit = flush.console, print = FALSE)
  trace(what = "cat", where = getNamespace("base"), exit = flush.console, print = FALSE)

  # Print system output

  # Store original system function
  base_system <- base::system
  # Define new system function that calls the original one and then prints
  capture_system <- function (x, ...) {
    # First store original intent, then overwrite `intern`
    args <- list(...) # Beware: expressions in ... are evaluated here
    if (is.null(args$intern)) {
      intent_intern <- NA
    } else if (args$intern) {
      intent_intern <- TRUE
    } else {
      intent_intern <- FALSE
    }
    args$intern <- TRUE

    # Catch error but rethrow if intended
    tryCatch({
      res <- withr::with_options(
        list(warn = if (isFALSE(intent_intern)) -1 else 1),
        do.call(what = base_system, args = c(list(x), args))
      )

      if (isTRUE(intent_intern)) {
        return(res)
      } else {
        # If not explicitly prevented: print result
        if (is.na(intent_intern)) cat(res, sep = "\n")
        return(invisible(attr(res, "status")))
      }
    }, error = function(cond) {
      if (isTRUE(intent_intern)) stop(cond) else return(invisible(127L))
    })
  }
  # Retrieve the namespace environment of {base}
  ns_env <- rlang::ns_env(x = "base")
  # Allow the new system function to access internals from {base}
  environment(capture_system) <- ns_env
  # Store the new system function in the namespace environment
  unlockBinding("system", ns_env)
  assign(x = "system", value = capture_system, envir = ns_env)
  lockBinding("system", ns_env)

  # Store original system2 function
  base_system2 <- base::system2
  # Define new system2 function that calls the original one and then prints
  capture_system2 <- function (x, ...) {
    # First store original intent, then overwrite `stdout` and `stderr`
    args <- list(...) # Beware: expressions in ... are evaluated here
    if (is.null(args$stdout) && is.null(args$stderr)) {
      intent_intern <- NA
    } else if (isTRUE(args$stdout) || isTRUE(args$stderr)) {
      intent_intern <- TRUE
    } else {
      intent_intern <- FALSE
    }
    args$stdout <- TRUE
    args$stderr <- TRUE

    # Catch error but rethrow if intended
    tryCatch({
      res <- withr::with_options(
        list(warn = if (isFALSE(intent_intern)) -1 else 1),
        do.call(what = base_system2, args = c(list(x), args))
      )

      if (isTRUE(intent_intern)) {
        return(res)
      } else {
        # If not explicitly prevented: print result
        if (is.na(intent_intern)) cat(res, sep = "\n")
        return(invisible(attr(res, "status")))
      }
    }, error = function(cond) {
      if (isTRUE(intent_intern)) stop(cond) else return(invisible(127L))
    })
  }
  # Retrieve the namespace environment of {base}
  ns_env <- rlang::ns_env(x = "base")
  # Allow the new system2 function to access internals from {base}
  environment(capture_system2) <- ns_env
  # Store the new system2 function in the namespace environment
  unlockBinding("system2", ns_env)
  assign(x = "system2", value = capture_system2, envir = ns_env)
  lockBinding("system2", ns_env)
}
